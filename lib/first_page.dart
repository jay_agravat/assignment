import 'package:assignment_2023/second_page.dart';
import 'package:assignment_2023/third_page.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({super.key});

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  var selectedIndex;
  List<Map> users = [];
  List<Map> items = [];
  List<Map> restaurants = [];

  void initUsers() {
    Map<String, dynamic> map = {};
    map['Name'] = 'Fast Food';
    map['img'] = 'assets/images/fast_food.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Fruites';
    map['img'] = 'assets/images/frueit.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Pizza';
    map['img'] = 'assets/images/Pizza.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Sweet';
    map['img'] = 'assets/images/sweet.jpeg';
    users.add(map);

    map = {};
    map['Name'] = 'Cold Drinks';
    map['img'] = 'assets/images/Cold_drinks.jpeg';
    users.add(map);
  }

  void initItems() {
    Map<String, dynamic> item = {};
    item['Name'] = 'Paneer Handi';
    item['test'] = 'Spicy Paneer';
    item['price'] = '\$12.00';
    item['img'] = 'assets/images/1_panner_handi.jpeg';
    item['isfav'] = true;
    items.add(item);

    item = {};
    item['Name'] = 'Paneer Tufani';
    item['test'] = 'Fresh Paneer';
    item['price'] = '\$23.00';
    item['img'] = 'assets/images/1_panner_tufani.jpeg';
    item['isfav'] = false;
    items.add(item);

    item = {};
    item['Name'] = 'Kadai Paneer';
    item['test'] = 'Spicy Kadhai';
    item['price'] = '\$25.00';
    item['img'] = 'assets/images/1_kadai_paneer.jpeg';
    item['isfav'] = false;
    items.add(item);

    item = {};
    item['Name'] = 'Paneer Bhurji';
    item['test'] = 'Delicious Bhurji';
    item['price'] = '\$20.00';
    item['img'] = 'assets/images/1_panner_bhurji.jpeg';
    item['isfav'] = false;
    items.add(item);

    item = {};
    item['Name'] = 'Mix veg.';
    item['test'] = 'Fresh veg.';
    item['price'] = '\$22.00';
    item['img'] = 'assets/images/1_mix_veg.jpeg';
    item['isfav'] = false;
    items.add(item);
  }

  void initRestaurants() {
    Map<String, dynamic> restaurant = {};
    restaurant['Name'] = 'Foodcave Restaurant';
    restaurant['img'] = 'assets/images/restaurants_1.jpeg';
    restaurant['location'] = 'New York, Australia';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Momai Restaurant';
    restaurant['img'] = 'assets/images/restaurants_2.jpeg';
    restaurant['location'] = 'Rajkot, India';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Cheesy Restaurant';
    restaurant['img'] = 'assets/images/restaurants_3.jpeg';
    restaurant['location'] = 'california, US';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Cafe Yum Restaurant';
    restaurant['img'] = 'assets/images/restaurants_4.jpeg';
    restaurant['location'] = 'New York, Australia';
    restaurants.add(restaurant);

    restaurant = {};
    restaurant['Name'] = 'Thai Tanic Restaurant';
    restaurant['img'] = 'assets/images/restaurants_5.jpeg';
    restaurant['location'] = 'hong kong ,disneyland';
    restaurants.add(restaurant);
  }

  @override
  void initState() {
    super.initState();
    initUsers();
    initItems();
    initRestaurants();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Center(
          child: Text(
            "Search Food",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black87,
            ),
          ),
        ),
        leading: InkWell(
          child: const Icon(
            Icons.menu,
            color: Colors.black87,
          ),
        ),

        // drawer: Drawer(
        //   // semanticLabel: 'Jay',
        //   backgroundColor: Colors.black26,
        //   // child: Icon(Icons.menu),
        // ),
        actions: [
          Container(
            margin: EdgeInsets.fromLTRB(20, 7, 20, 7),
            child: CircleAvatar(
              backgroundImage: AssetImage(
                'assets/images/first_page_top_photo.png',
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                height: 45,
                margin: EdgeInsets.all(20),
                // width: 700,
                color: Colors.white70,
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Container(
                        // width: 45,
                        // color: Colors.black12,
                        child: TextField(
                          style: TextStyle(
                            color: Colors.black87,
                            // backgroundColor: Colors.white70,
                          ),
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.black26,
                            floatingLabelAlignment:
                                FloatingLabelAlignment.center,
                            prefixIcon: Icon(Icons.search),
                            hintText: 'Healty Food',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: SizedBox(
                        width: 20,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 80,
                        // width: 10,
                        // color: Colors.red,
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                        child: Icon(Icons.tune_outlined),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                // margin: EdgeInsets.only(top: 20),
                height: 45,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(right: 10),
                      padding: EdgeInsets.only(left: 10),
                      width: 140,
                      height: 30,
                      decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                            radius: 15,
                            backgroundImage: AssetImage(
                              users[index]['img'].toString(),
                              // 'assets/images/first_page_top_photo.png',
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            users[index]['Name'],
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: users.length,
                ),
              ),
              Container(
                height: 221,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) {
                              return SecondPage(
                                foodImage: items[index]['img'],
                                foodName: items[index]['Name'],
                                foodPrice: items[index]['price'],
                                isFav: items[index]['isfav'],
                              );
                            },
                          ),
                        );
                      },
                      child: Container(
                        width: 120,
                        margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                        padding: EdgeInsets.all(7),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 8,
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Container(
                              // width: double.infinity,
                              // height: 100,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(30),
                                child: Image.asset(
                                  height: 80,
                                  items[index]['img'].toString(),
                                  // fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              items[index]['Name'],
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(items[index]['test']),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(items[index]['price']),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      items[index]['isfav'] =
                                          !items[index]['isfav'];
                                    });
                                  },
                                  child: Icon(
                                    items[index]['isfav']
                                        ? Icons.favorite
                                        : Icons.favorite_border,
                                    color: Colors.red,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: items.length,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Favorite Restaurants',
                    style: TextStyle(fontWeight: FontWeight.w700),
                  ),
                  Text(
                    'See all',
                    style: TextStyle(
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Container(
                height: 130,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      width: 267,
                      margin: EdgeInsets.fromLTRB(0, 10, 20, 10),
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black26,
                            blurRadius: 8,
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Container(
                                // width: double.infinity,
                                // height: 100,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage(
                                    restaurants[index]['img'].toString(),
                                  ),
                                  radius: 40,
                                ),
                                margin: EdgeInsets.all(6),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    restaurants[index]['Name'],
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    restaurants[index]['location'],
                                    style: TextStyle(
                                        // fontWeight: FontWeight.w700,
                                        ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  RatingBar.builder(
                                    itemSize: 20,
                                    glowColor: Colors.red,
                                    initialRating: 3,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemPadding: EdgeInsets.only(right: 5.0),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                  itemCount: items.length,
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: CurvedNavigationBar(
          backgroundColor: Colors.deepPurple,
          color: Colors.deepPurple.shade100,
          items: [
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: ((context) {
                  return SecondPage();
                })));
              },
              child: Icon(Icons.home_filled),
            ),
            const Icon(CupertinoIcons.profile_circled),
            const Icon(CupertinoIcons.heart),
            InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) {
                    return const ThirdPage();
                  },
                ));
              },
              child: Icon(CupertinoIcons.shopping_cart),
            ),
          ]),
      // bottomNavigationBar: Padding(
      //   padding: EdgeInsets.all(8.0),
      //   child: ClipRRect(
      //     borderRadius: BorderRadius.circular(10),
      //     child: BottomNavigationBar(
      //       // fixedColor: Colors.red,
      //       backgroundColor: Colors.black,
      //       // type: BottomNavigationBarType.fixed,
      //       unselectedIconTheme: IconThemeData(color: Colors.white),
      //       selectedIconTheme: IconThemeData(color: Colors.amber),
      //       currentIndex: 0,
      //       items: [
      //         BottomNavigationBarItem(
      //           icon: Container(
      //             padding: EdgeInsets.all(5),
      //             margin: EdgeInsets.only(top: 10),
      //             decoration: BoxDecoration(
      //               // color: Colors.white,
      //               border: Border(
      //                 bottom: BorderSide(color: Colors.amber)
      //               ),
      //             ),
      //             child: Icon(Icons.add),
      //           ),
      //           backgroundColor: Colors.black,
      //           label: "",
      //         ),
      //         BottomNavigationBarItem(
      //           icon: Container(
      //             child: new Icon(Icons.account_box_outlined),
      //           ),
      //           backgroundColor: Colors.white,
      //           label: "",
      //         ),
      //         BottomNavigationBarItem(
      //           icon: new Icon(Icons.account_box_outlined),
      //           backgroundColor: Colors.white,
      //           label: "",
      //         ),
      //         BottomNavigationBarItem(
      //           icon: new Icon(Icons.delete),
      //           backgroundColor: Colors.white,
      //           label: "",
      //         ),
      //       ],
      //       onTap: (index) {
      //         setState(
      //           () {
      //             selectedIndex = index;
      //           },
      //         );
      //       },
      //     ),
      //   ),
      // ),
    );
  }
}
