import 'package:assignment_2023/third_page.dart';
import 'package:flutter/material.dart';

class SecondPage extends StatefulWidget {
  dynamic foodImage;
  dynamic foodName;
  dynamic foodPrice;
  dynamic isFav;

  SecondPage(
      {super.key, this.foodImage, this.foodName, this.foodPrice, this.isFav});

  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  @override
  void initState() {
    print("Fav: ${widget.isFav}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.start,\
            children: [
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          margin: EdgeInsets.only(left: 8),
                          child: Icon(Icons.arrow_back_ios),
                          width: 20,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        setState(() {
                          widget.isFav = !widget.isFav;
                        });
                      },
                      child: Icon(
                        widget.isFav ? Icons.favorite : Icons.favorite_border,
                        color: Colors.red,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  height: 200,
                  alignment: Alignment.center,
                  widget.foodImage.toString(),
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          top: 7, right: 20, bottom: 7, left: 20),
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Icon(Icons.star,color: Colors.amber,size: 20,),
                          Text("5.0"),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 7, right: 20, bottom: 7, left: 20),
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          Icon(Icons.remove,size: 20,),
                          Text("02"),
                          Icon(Icons.add,size: 20,),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Stack(
                children: [
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        color: Colors.white30,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20))),
                    height: 300,
                  ),
                  Padding(
                    padding:  EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //   children: [
                        //     Container(
                        //       padding: EdgeInsets.only(
                        //           top: 7, right: 20, bottom: 7, left: 20),
                        //       decoration: BoxDecoration(
                        //         color: Colors.grey,
                        //         borderRadius: BorderRadius.circular(10),
                        //       ),
                        //       child: Row(
                        //         children: [
                        //           Icon(Icons.star,color: Colors.amber,size: 20,),
                        //           Text("5.0"),
                        //         ],
                        //       ),
                        //     ),
                        //     Container(
                        //       padding: EdgeInsets.only(
                        //           top: 7, right: 20, bottom: 7, left: 20),
                        //       decoration: BoxDecoration(
                        //         color: Colors.amber,
                        //         borderRadius: BorderRadius.circular(10),
                        //       ),
                        //       child: Row(
                        //         children: [
                        //           Icon(Icons.remove,size: 20,),
                        //           Text("02"),
                        //           Icon(Icons.add,size: 20,),
                        //         ],
                        //       ),
                        //     )
                        //   ],
                        // ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              widget.foodName.toString(),
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 20
                              ),
                            ),
                            // Icon(Icons.access_alarm),
                            Row(
                              children: [
                                Icon(Icons.access_alarm,color: Colors.red,),
                                Text("10-15 Mins"),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                            "Grille Meat Skewers , shis kebeb and health to  vegetable salad cucumbe"),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Toping For You",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              "Clear",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          height: 50,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: [
                              ProductMaker(
                                'assets/images/Pizza.jpeg',
                              ),
                              ProductMaker('assets/images/frueit.jpeg'),
                              ProductMaker('assets/images/fast_food.jpeg'),
                              ProductMaker('assets/images/restaurants_1.jpeg'),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Tottal Price"),
                                Text(widget.foodPrice.toString())
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top: 5, right: 20, bottom: 5, left: 20),
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  )),
                              child: InkWell(
                                onTap: () {
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) {
                                        return ThirdPage();
                                      },
                                    ),
                                  );
                                },
                                child: Row(
                                  children: [
                                    Icon(Icons.shopping_cart,
                                        color: Colors.white),
                                    Text(
                                      "Add To Cart",
                                      style: TextStyle(color: Colors.white),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget ProductMaker(image) {
    return Container(
      width: 50,
      height: 50,
      margin: EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Image.asset(
        image,
        fit: BoxFit.fill,
      ),
    );
  }
}
